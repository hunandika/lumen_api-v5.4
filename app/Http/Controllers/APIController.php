<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Application;
use Illuminate\Http\JsonResponse;
use App\Http\Models\Users;
use Illuminate\Http\Request;

class APIController extends Controller
{
    /**
     * Get root url.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex(Application $app)
    {
        return new JsonResponse(['message' => $app->version()]);
    }

    public function getUser()
    {
        $user = Users::get();
        $message = response()->json(array(
            'result' => $user,
            'status' => true,
            'message' => ''
              ));
          return $message;
    }

    public function insertUser(Request $request)
    {
        $user = new Users;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = app('hash')->make($request->password);
        $user->remember_token = str_random(10);
        $user->save();

        $message = response()->json(array(
            'result' => $user,
            'status' => true,
            'message' => 'save user success'
              ));
          return $message;
    }

    public function updateUser(Request $request)
    {
        $user = Users::find($request->id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = app('hash')->make($request->password);
        $user->remember_token = str_random(10);
        $user->update();

        $message = response()->json(array(
            'result' => $user,
            'status' => true,
            'message' => 'update user success'
              ));
          return $message;
    }

    public function hardDeleteUser(Request $request)
    {
        $user = Users::find($request->id);
        $user->delete();

        $message = response()->json(array(
            'result' => $user,
            'status' => true,
            'message' => 'delete user success'
              ));
          return $message;
    }
}
