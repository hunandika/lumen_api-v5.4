<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exception\HttpResponseException;

class AuthController extends Controller
{
    /**
     * Handle a login request to the application.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function postLogin(Request $request)
    {
        try {
            $this->validate($request, [
                'email' => 'required|email|max:255',
                'password' => 'required',
            ]);
        } catch (ValidationException $e) {
            return $e->getResponse();
        }


        try {
            $credentials = $request->only('email','password');
            $token = null;
            // Attempt to verify the credentials and create a token for the user
            if (!$token = JWTAuth::attempt($credentials)) {
                $message = response()->json(array(
                    'status' => False,
                    'message' => 'Username atau password salah',
                      ));
                return $message;
            }
        } catch (JWTException $e) {
            // Something went wrong whilst attempting to encode the token
            $message = response()->json(array(
                'status' => False,
                'error' => 'Gagal mendapatkan token',
                  ));
            return $message;
        }

        $user = JWTAuth::toUser($token);

        $message = response()->json(array(
            //  'tampil_bantuan' => $tampil_bantuan,
              'id' =>$user->id,
              'name' =>$user->name,
              'email' =>$user->email,
              'token'=>$token,
              'status' => True,
              'error' => '200',
                ));
          return $message;
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\Response
     */
    public function patchRefresh()
    {
        $token = JWTAuth::parseToken();

        $newToken = $token->refresh();

        return new JsonResponse([
            'message' => 'token_refreshed',
            'token' => $newToken,
            'status' => True            
        ]);
    }
}
