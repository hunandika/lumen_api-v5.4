<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$api = app(Dingo\Api\Routing\Router::class);

$api->version('v1', function ($api) {
    $api->post('/auth/login', [
        'as' => 'api.auth.login',
        'uses' => 'App\Http\Controllers\Auth\AuthController@postLogin',
    ]);
    $api->get('/', [
        'uses' => 'App\Http\Controllers\APIController@getIndex',
        'as' => 'api.index'
    ]);

    $api->group([
        'middleware' => 'api.auth',
    ], function ($api) {
      
        $api->get('/getuser', ['uses' => 'App\Http\Controllers\APIController@getUser']);
        $api->post('/insertuser', ['uses' => 'App\Http\Controllers\APIController@insertUser']);
        $api->post('/updateuser', ['uses' => 'App\Http\Controllers\APIController@updateUser']);
        $api->post('/deleteuser', ['uses' => 'App\Http\Controllers\APIController@hardDeleteUser']);
        $api->post('/auth/refresh', ['uses' => 'App\Http\Controllers\Auth\AuthController@patchRefresh']);
    });
});
