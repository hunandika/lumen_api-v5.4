# Lumen with JWT Authentication
Basically this is a starter kit for you to integrate Lumen with [JWT Authentication](https://jwt.io/).

## What's Added

- [Lumen 5.4](https://github.com/laravel/lumen/tree/v5.4.0).
- [JWT Auth](https://github.com/tymondesigns/jwt-auth) for Lumen Application. 
- [Dingo](https://github.com/dingo/api) to easily and quickly build your own API. 
- [Lumen Generator](https://github.com/flipboxstudio/lumen-generator) to make development even easier and faster.
- [CORS and Preflight Request](https://developer.mozilla.org/en-US/docs/Web/HTTP/Access_control_CORS) support.

## Quick Start

- Create folder 'lumen_api' to your htdocs directory
- Clone this repo or download it's release archive and extract it to folder 'lumen_api'
- You may delete `.git` folder if you get this code via `git clone`
- Run `composer install`
- Rename `.env.example` to `.env` file for config project
- Configure your `.env` file for authenticating via database
- Run `php artisan jwt:generate` and copy key to `.env` JWT_SECRET
- Set the `API_PREFIX` parameter in your .env file (usually `api`).
- Run `php artisan migrate --seed`


## ETC

This Laravel lumen without folder public, you can run it at domainName.com/api/apicollection
To try this API we make collection on Postman [here](https://www.getpostman.com/collections/4c2015480bb369e24651).

## License

```
Laravel and Lumen is a trademark of Taylor Otwell
Sean Tymon officially holds "Laravel JWT" license
```

Thanks.
Hunandika Adyota Purba
2018